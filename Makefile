h: help
d: dev
b: build
dist:build # agregar paquetes estaticos como imagenes

help:
	@echo "make dependencies init # instala las depdendencias para este proyecto"
	@echo "make d # Lanzar el entorno de desarrollo"
	@echo "make b # Generar los archivos comprimidos para la interfaz gráfica del código"

dependencies:
	nvm install v21.7.1 # versión de nodejs actualizada
	nvm use v21.7.1
	npm install -g npm-check-updates

init:
	npm-check-updates
	ncu -u
	npm install

desktop:
	rm -rf src/App.vue
	ln -sr src/App-desktop.vue src/App.vue

android:
	# Agrega funcionalidades de conexión y uso con android como es el caso de las conexiones TCP
	rm -rf src/App.vue
	ln -sr src/App-android.vue src/App.vue

dev: dist
	rm -rf ./src/blockly-config/chuck
	rm -rf ./src/blockly-config/luna8266
	# ln -sr ../../catalejo-oursblockly/targets/chuck/dist/chuck ./src/blockly-config/
	# ln -sr ../../catalejo-oursblockly/targets/luna8266/dist/luna8266 ./src/blockly-config/
	cp -var ../../catalejo-oursblockly/targets/chuck/dist/chuck ./src/blockly-config/
	cp -var ../../catalejo-oursblockly/targets/luna8266/dist/luna8266 ./src/blockly-config/
	npm run dev

build:
	@echo "Copiando contenido de chuck y luna8266 para configuraciones de blockly"
	rm -rf ./src/blockly-config/chuck
	rm -rf ./src/blockly-config/luna8266
	cp -var ../../catalejo-oursblockly/targets/chuck/dist/chuck ./src/blockly-config/
	cp -var ../../catalejo-oursblockly/targets/luna8266/dist/luna8266 ./src/blockly-config/
	@echo "Creando carpeta para documentación de luna8266"
	mkdir -p ./dist/luna8266-docs
	@echo "Creando carpeta para documentación de chuck"
	mkdir -p ./dist/chuck-docs
	@echo "Empaquetando"
	npm run build-only

send2nwjs:
	rm -rf ../../catalejo-nwjs/build-nwjs/nwjs-v0.60.0-linux-x64/dist
	cp -var dist ../../catalejo-nwjs/build-nwjs/nwjs-v0.60.0-linux-x64/

references:
	firefox
