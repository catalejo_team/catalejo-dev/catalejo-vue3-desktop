// -- START CODIFICACIÓN Y DECODIFICACIÓN DE BUFFER-STRING --

// https://developer.chrome.com/blog/how-to-convert-arraybuffer-to-and-from-string/
/*
 * @function str2ab
 * Convertir string a array buffer
 */
export function str2ab(str: string) {
  const buf = new ArrayBuffer(str.length); // 2 bytes for each char
  const bufView = new Uint8Array(buf);
  for (let i = 0, strLen = str.length; i < strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}

// https://developer.chrome.com/blog/how-to-convert-arraybuffer-to-and-from-string/
/*
 * @function ab2str
 * Convertir array buffer a string
 */
export function ab2str(buf: ArrayBuffer) {
  return String.fromCharCode.apply(null, new Uint8Array(buf));
}
// -- END CODIFICACIÓN Y DECODIFICACIÓN DE BUFFER-STRING --
