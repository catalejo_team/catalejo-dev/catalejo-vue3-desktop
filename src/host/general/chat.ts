// https://stackblitz.com/github/nvima/chat-ui/tree/main/dev/vue3-npm?file=src%2FApp.vue
// https://github.com/nvima/chat-ui

import { ref } from "vue";

const limitArray = 115;
const slice = -15;
let counter = 0;

export const dataChat = ref([
  {
    message: "¡Bienvenido a Catalejo!",
    type: "chatbot",
    timestamp: formatAMPM(new Date()),
  },
]);

function updateDataChat(message) {
  dataChat.value.push(message);
  if (counter >= limitArray) {
    dataChat.value = dataChat.value.slice(slice);
    counter = 0;
  }
  counter++;
}

function formatAMPM(date: Date) {
  let hours = date.getHours();
  let minutes = date.getMinutes();
  const ampm = hours >= 12 ? "PM" : "AM";
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? 0 + minutes : minutes;
  const strTime = hours + ":" + minutes + " " + ampm;
  return strTime;
}

export function handleChatSendEvent(input: string) {
  if (input == "") return;
  const messagePerson = {
    type: "person",
    timestamp: formatAMPM(new Date()),
    message: input,
  };
  updateDataChat(messagePerson);
}

export function handleChatLog(input: string) {
  if (input == "") return;
  const messageLog = {
    type: "chatbot",
    timestamp: formatAMPM(new Date()),
    message: input,
  };
  updateDataChat(messageLog);
}
