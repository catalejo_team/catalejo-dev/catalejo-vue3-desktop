export function log(data) {
  console.log(data);
}

export function debug(data) {
  console.debug(data);
}

export function error(data) {
  console.error(data);
}

export function terminal(debug){
  console.info(data);
}

// https://vue3-toastify.js-bridge.com/
import "vue3-toastify/dist/index.css";
import { toast } from "vue3-toastify";

interface ToasTConfig {
  data: string | number | object | content;
  type?: ToastType;
  time?: number;
  logs?: Array<string>;
}

// https://vue3-toastify.js-bridge.com/
export function toastMsg(toastCfg: ToasTConfig) {
  toast(toastCfg.data, {
    autoClose: toastCfg.time ? toastCfg.time : 2000,
    theme: "dark",
    type: toastCfg.type ? toastCfg.type : "default",
  });
  if (toastCfg.logs != undefined) {
    for (const thisConsole in toastCfg.logs) {
      switch (toastCfg.logs[thisConsole]) {
        case "log":
          log(toastCfg.data);
          break;
        case "debug":
          debug(toastCfg.data);
          break;
        case "error":
          error(toastCfg.data);
          break;
        case "terminal":
          terminal(toastCfg.data);
          break;

        default:
          break;
      }
    }
  }
}

export enum StatusTask {
  SUCCESS,
  INPROCESS,
  ERROR,
  IDLE,
}
