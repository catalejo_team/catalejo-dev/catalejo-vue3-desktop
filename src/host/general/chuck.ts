// Definiciones genéricas de chuck

export enum ChuckStatus {
  LOCAL_CONNECT = 0,
  DISCONNECT = 1,
  REMOTE_CONNECT = 2,
}
