import * as nwjs from "../nwjs/fileManager";
import * as webjs from "../browser/fileManagerWebApi";

let host = "web";

if (nw.process.platform != undefined) {
  host = "nw";
}

export async function getFileHandle(file: File) {
  if (host == "nw") {
    return await nwjs.getFileHandle(file);
  } else if (host == "web") {
    return await webjs.getFileHandle();
  }
}

import type { CatalejoDataPrj } from "../defCatalejo";

export async function saveCatalejoPrj(
  fileHandle: object,
  catalejoPrj: CatalejoDataPrj
) {
  if (host == "nw") {
    return await nwjs.saveCatalejoPrj(fileHandle, catalejoPrj);
  }
}

export async function updateEditorAndBlockly(fileHandle: object) {
  if (host == "nw") {
    return await nwjs.updateEditorAndBlockly(fileHandle);
  }
}

export async function getNewFileHandle() {
  if (host == "nw") {
    return await nwjs.getNewFileHandle();
  }
}
