import * as nwjs from "../nwjs/tcpSocketLuna8266";

// TODO eliminar este controllador

let host = "web";

if (nw.process.platform != undefined) {
  host = "nw";
}

export async function saveCodeInLuna8266(ip: string, port: number, code: string) {
  if (host == "nw") {
    return await nwjs.saveCodeInLuna8266(ip, port, code);
  }
}
