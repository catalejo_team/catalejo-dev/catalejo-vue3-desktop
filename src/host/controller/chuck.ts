import * as nwjs from "../nwjs/chuck";
export * from "../general/chuck";

let host = "web"; //[web, nw]

if (nw.process.platform != undefined) {
  host = "nw";
}

/**
 * Metodos de conexión al servicio de chuck
 */
export enum ConnectChuckMethod {
  LOCAL,
  REMOTE,
}

/**
 * Metodos de conexión a un servicio chuck soportados por nwjs
 */
const NWJSConnectChuckMethodEnabled = [
  ConnectChuckMethod.LOCAL,
  ConnectChuckMethod.REMOTE,
];

let connectChuckMethodSelected = ConnectChuckMethod.LOCAL;

export function getConnectChuckMethodSelected() {
  return connectChuckMethodSelected;
}

export function setConnectChuckMethodSelected(method: ConnectChuckMethod) {
  connectChuckMethodSelected = method;
}

export function connectChuckMethodSupport(
  connectChuckMethodRequest: ConnectChuckMethod
) {
  if (host == "nw") {
    for (const method in NWJSConnectChuckMethodEnabled) {
      if (NWJSConnectChuckMethodEnabled[method] == connectChuckMethodRequest) {
        return true; // Supported method
      }
    }
    return false; // Dont support this method
  }
}

// -- START MANEJO DE ARCHIVOS --

export async function getPathChuckFile(
  fileHandle: File,
  typeEditor: number,
  position: number
) {
  if (host == "nw") {
    return await nwjs.getPathChuckFile(fileHandle, typeEditor, position);
  }
}

export async function saveChuckFile(file: object | string, content: string) {
  if (host == "nw") {
    if (typeof file === "string") {
      try {
        await nwjs.saveChuckFile(file, content);
        return "Archivo guardado";
      } catch (err) {
        return "Error al tratar de guardar en el archivo";
      }
    }
  }
}
// -- START MANEJO DE ARCHIVOS --

// -- COMANDOS CHUCK --

export async function startChuck() {
  if (host == "nw") {
    return await nwjs.startChuck();
  }
}

export async function killChuck() {
  if (host == "nw") {
    return await nwjs.killChuck();
  }
}

export async function addScriptChuck(file: object | string): Promise<string> {
  if (host == "nw") {
    if (typeof file === "string") {
      return await nwjs.addScriptChuck(file);
    }
  }
}

export async function removeTheLastScripChuck() {
  if (host == "nw") {
    return await nwjs.removeTheLastScripChuck();
  }
}

export async function removeAllScriptChuck() {
  if (host == "nw") {
    return await nwjs.removeAllScriptChuck();
  }
}

export let chuckConnectIP: nwjs.ChuckConnectIP;

if (host == "nw") {
  chuckConnectIP = new nwjs.ChuckConnectIP();
}

export async function checkChuckRemoteConection(ip: string) {
  if (host == "nw") {
    return await nwjs.checkChuckRemoteConection(ip);
  }
}

export async function setOptionChuck(ip: Array<string>) {
  if (host == "nw") {
    return await nwjs.setOptionChuck(ip);
  }
}

export function getStatusChuck() {
  if (host == "nw") {
    return nwjs.getStatusChuck();
  }
}
