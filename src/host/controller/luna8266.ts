import * as nwjsTCP from "../nwjs/tcpSocketLuna8266";
import * as nwjsUSB from "../nwjs/usbLuna8266";
import { toastMsg } from "../general/logInfo";

let host = "web"; //[web, nw]

if (nw.process.platform != undefined) {
  host = "nw";
}

/**
 * Metodos de envío de código a la tarjeta nodemcu
 */
export enum ShippingMethod {
  STAWIFI,
  APWIFI,
  USB,
}

// Supported shipping methods by nwjs
const NWJSShippingMethodEnabled = [
  ShippingMethod.APWIFI,
  ShippingMethod.STAWIFI,
  ShippingMethod.USB,
];

class Luna8266 {
  ssidSTA: string;
  ipSTA: string;
  ipAP = "192.168.4.1";
  port = 1522;
  shippingMethodSelected = ShippingMethod.APWIFI;

  constructor() {
    this.ssidSTA = "";
    this.ipSTA = "";
  }
  // Verificando si un metodo es soportado
  shippingMethodSupport(shippingMethodRequest: ShippingMethod) {
    if (host == "nw") {
      for (const method in NWJSShippingMethodEnabled) {
        if (NWJSShippingMethodEnabled[method] == shippingMethodRequest) {
          return true; // Supported method
        }
      }
      return false; // Dont support this method
    }
  }
  // metodo para enviar código a la tarjeta nodemcu
  async saveCodeInLuna8266(code: string) {
    if (host == "nw") {
      switch (this.shippingMethodSelected) {
        case ShippingMethod.APWIFI:
          return await nwjsTCP.saveCodeInLuna8266(this.ipAP, this.port, code);
          break;
        case ShippingMethod.STAWIFI:
          return await nwjsTCP.saveCodeInLuna8266(this.ipSTA, this.port, code);
          break;
        case ShippingMethod.USB:
          return await nwjsUSB.saveCodeInLuna8266(code);
          break;
        default:
      }
    }
  }
  // método para comprobar conexión TCP con luna8266
  async testConnectLuna8266() {
    if (host == "nw") {
      switch (this.shippingMethodSelected) {
        case ShippingMethod.APWIFI:
          return await nwjsTCP.testTCPConnect(this.ipAP, this.port, "test\n");
          break;
        case ShippingMethod.STAWIFI:
          if (this.ipSTA == "") {
            toastMsg({
              data: "Poner una IP válida para realizar la conexión",
              type: "error",
            });
            return false;
          }
          return await nwjsTCP.testTCPConnect(this.ipSTA, this.port, "test\n");
          break;
        default: // TODO: No fue posible agregar USB por overloading method
      }
    }
  }
  async testUSBConnectLuna8266(path: string) {
    if (host == "nw") {
      return await nwjsUSB.testUSBConnect(path);
    }
  }
  async usbDisconnect() {
    if(host == "nw") {
      return await nwjsUSB.usbDisconnect();
    }
  }
  async scanEsp8266ByIP() {
    if (host == "nw") {
      switch (this.shippingMethodSelected) {
        case ShippingMethod.STAWIFI:
          return await nwjsTCP.scanDeviceByIp();
          break;
        default:
      }
    }
  }
  // Obtener lista de IPs válidas de esp8266 con
  // servicio Lua
  async getValideEsp8266IpList() {
    if (host == "nw") {
      switch (this.shippingMethodSelected) {
        case ShippingMethod.STAWIFI:
          return await nwjsTCP.getValideEsp8266IpList();
          break;
        default:
      }
    }
  }
  // Método para detenerl el test inmediatamente
  async stopTestConnectLuna8266() {
    if (host == "nw") {
      switch (this.shippingMethodSelected) {
        case ShippingMethod.STAWIFI:
        case ShippingMethod.APWIFI:
          return await nwjsTCP.tcpClose();
          break;
        default: // TODO: agregar USB metodo
      }
    }
  }
  // Estado de la tarea de la conexión con el esp8266
  getStatusConnectLuna() {
    if (host == "nw") {
      switch (this.shippingMethodSelected) {
        case ShippingMethod.STAWIFI:
        case ShippingMethod.APWIFI:
          return nwjsTCP.getStatusConnectLunaTCP();
        case ShippingMethod.USB:
          return nwjsUSB.getStatusConnectLunaUSB();
          break;
        default: // TODO: agregar USB metodo
      }
    }
  }
  // Actualizar ip e issd válidos
  async updateIPSTAandSSID() {
    if (host == "nw") {
      switch (this.shippingMethodSelected) {
        case ShippingMethod.STAWIFI:
        case ShippingMethod.APWIFI:
          this.ssidSTA = await nwjsTCP.getSSID();
          this.ipSTA = await nwjsTCP.getIPSTA();
          return;
          break;
        default: // TODO: agregar USB metodo
      }
    }
  }
  // USB get Serialport list
  async getSerialPortList() {
    if (host == "nw") {
      return nwjsUSB.getSerialPortList();
    }
  }
  // -- GETS AND SETTERS --
  getShippinMethodSelected() {
    return this.shippingMethodSelected;
  }
  setShippinMethod(shippingMethod: ShippingMethod) {
    this.shippingMethodSelected = shippingMethod;
  }
  getIPSTA() {
    return this.ipSTA;
  }
  setIPSTA(ipSTA: string) {
    this.ipSTA = ipSTA;
  }
  getIPAP() {
    return this.ipAP;
  }
  setIPAP(ipAP: string) {
    this.ipAP = ipAP;
  }
  getSsidSTA() {
    return this.ssidSTA;
  }
  setSsidSTA(ssidSTA: string) {
    this.ssidSTA = ssidSTA;
  }
}

export const luna8266 = new Luna8266();
