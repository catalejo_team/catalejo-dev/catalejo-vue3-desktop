import * as nwjs from "../nwjs/civetweb";

let host = "web"; //[web, nw]

if (nw.process.platform != undefined) {
  host = "nw";
}

export async function startWebservice() {
  if (host == "nw") {
    return await nwjs.startCivetweb();
  }
}

export function getWebserviceState() {
  if (host == "nw") {
    return nwjs.getCivetwebState();
  }
}

export { WebserviceStatus } from "../general/webservice";
