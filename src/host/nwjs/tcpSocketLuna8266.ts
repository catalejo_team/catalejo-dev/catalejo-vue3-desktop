// How USE chrome.sockets
// https://www.oreilly.com/library/view/programming-chrome-apps/9781491905272/ch04.html
import { toastMsg, debug, StatusTask} from "../general/logInfo";
const tcp = chrome.sockets.tcp;

export let createInfo: chrome.sockets.CreateInfo;
export let sockectLuna: number;

export let msgReceived: string; // mensaje recibido por parte del esp8266
export let msgReceivedCounter: number; // cantidad de mensajes recibidos desde el esp8266
export let ssidOfesp8266: string; // Nombre de la ssid a la que está conectada el esp8266
export let ipStaEsp8266: string; // ip sta del esp8266
export let statusTCPConnect: StatusTask; // Seguimiento de la conexión ip con el esp8266

export async function getSSID() {
  return await ssidOfesp8266;
}

export async function getIPSTA() {
  return await ipStaEsp8266;
}

class CodeToSend {
  code: string;
  splitCode: Array<string>;
  lenSplitCode: number;
  constructor() {
    this.code = "";
    this.splitCode = [];
    this.lenSplitCode = 0;
  }
  setCode(code: string) {
    this.code = code;
    this.splitCode = code.split("\n");
    this.lenSplitCode = this.splitCode.length;
  }
  getLenSplitCode() {
    return this.lenSplitCode;
  }
  getCodeFromArray(index: number) {
    if (index < this.lenSplitCode) {
      return this.splitCode[index] + "\n";
    }
    return "undefined";
  }
  setCodeToSaveInLuna8266() {
    let tempCode = "";
    for (const indexCode in this.splitCode) {
      tempCode += "save__data" + this.splitCode[indexCode] + "\n";
    }
    tempCode = "save__begin\n" + tempCode + "save__end\n";
    // tempCode = "save__begin\n" + tempCode + "save__end\nsave__rs\n";
    this.splitCode = tempCode.split("\n");
    this.lenSplitCode = this.splitCode.length;
  }
}

const codeTosend = new CodeToSend();

export function getStatusConnectLunaTCP() {
  debug(statusTCPConnect);
  return statusTCPConnect;
}

// -- START process scan device for ip --
let validesp8266IpList: Array<string>; // Lista de ips válidas
let countSearch8266IP: number; // Contador de busquedas de Ip [0, 255]

export async function getValideEsp8266IpList() {
  // TODO: Dont require async
  return validesp8266IpList;
}

export async function scanDeviceByIp() {
  const ipUp = await getIpUpHost();
  validesp8266IpList = [];
  countSearch8266IP = 0;
  codeTosend.setCode("test");
  // await scanTCPCreate();
  const scanPromise = [];
  for (countSearch8266IP; countSearch8266IP < 256; countSearch8266IP++) {
    const ip = ipUp + countSearch8266IP;
    scanPromise.push(scanTCPConnect(ip));
    // await scanTCPConnect(ip);
  }
  await Promise.all(scanPromise).then(() => {
    debug("scan finish");
    debug(validesp8266IpList)
  });
  // tcpClose();
  debug(validesp8266IpList);
  return validesp8266IpList;
}

async function scanTCPConnect(ip: string) {
  return new Promise((resolveFunc) => {
    tcp.create({}, (thisCreateInfo: chrome.sockets.CreateInfo) => {
      try {
        tcp.connect(
          thisCreateInfo.socketId,
          ip,
          1522,
          (resultConnect: number) => {
            // TODO: esta función se ha quitado
            // debido a que la función es una
            // solicitud asyncrona y no se puede detener
            // toastMsg({
            //   data: "Buscando dispositivo en la ip: " + ip,
            //   type: "warning",
            //   time: 1000,
            // });
            debug("Buscando dispositivo en la ip: " + ip);
            if (resultConnect >= 0) {
              validesp8266IpList.push(ip);
              toastMsg({
                data: "Dispositivo encontrado en : " + ip,
                type: "success",
                time: 5000,
              });
              debug("ip valida: " + ip);
              resolveFunc(true);
            } else {
              resolveFunc(false);
            }
          },
        );
      } catch (e) {
        debug("ip: " + ip + " no alcanzable");
      }
    });
  });
}

function getIpUpHost() {
  const networkInterfaces = nw.require("os").networkInterfaces();
  // const keys = Object.keys(networkInterfaces);
  for (const key in networkInterfaces) {
    if (key != "lo") {
      const listIpOfInterface = networkInterfaces[key];
      for (const element in listIpOfInterface) {
        if (listIpOfInterface[element].family == "IPv4") {
          const ipSelect = listIpOfInterface[element].address;
          const ipSelectSplit = ipSelect.split(".");
          let ipUp = "";
          for (let i = 0, len = ipSelectSplit.length; i + 1 < len; i++) {
            ipUp = ipUp + ipSelectSplit[i] + ".";
          }
          return ipUp;
        }
      }
    }
  }
}

// TODO: Borrar si no es requerido 2023-11-04
// export async function scanDeviceByIp() {
//   const ipUp = await getIpUpHost();
//   validesp8266IpList = [];
//   countSearch8266IP = 0;
//   codeTosend.setCode("test");
//   await scanTCPCreate();
//   for (countSearch8266IP; countSearch8266IP < 256; countSearch8266IP++) {
//     const ip = ipUp + countSearch8266IP;
//     await scanTCPConnect(ip);
//   }
//   tcpClose();
//   debug(validesp8266IpList);
//   return validesp8266IpList;
// }

// TODO: borrar sino es requerido 2023-11-04
// async function scanTCPCreate() {
//   return new Promise((resolveFunc) => {
//     tcp.create({}, (thisCreateInfo: chrome.sockets.CreateInfo) => {
//       createInfo = thisCreateInfo;
//       if (createInfo != undefined && createInfo.socketId >= 0) {
//         resolveFunc(true);
//       }
//     });
//   });
// }

// TODO: borrar sino es requerido 2023-11-04
// async function scanTCPConnect(ip: string) {
//   return new Promise((resolveFunc) => {
//     tcp.connect(createInfo.socketId, ip, 1522, (resultConnect: number) => {
//       toastMsg({
//         data: "Buscando dispositivo en la ip: " + ip,
//         type: "warning",
//         time: 1000,
//       });
//       if (resultConnect >= 0) {
//         validesp8266IpList.push(ip);
//         toastMsg({
//           data: "Dispositivo encontrado en : " + ip,
//           type: "success",
//           time: 5000,
//         });
//         debug("ip valida: " + ip);
//         resolveFunc(true);
//       } else {
//         resolveFunc(false);
//       }
//     });
//   });
// }

// -- END process scan device for ip --

export async function testTCPConnect(ip: string, port: number, code: string) {
  statusTCPConnect = StatusTask.INPROCESS;
  codeTosend.setCode(code);
  tcpCreate(ip, port);
  return;
}

export async function saveCodeInLuna8266(
  ip: string,
  port: number,
  code: string
) {
  statusTCPConnect = StatusTask.INPROCESS;
  codeTosend.setCode(code);
  codeTosend.setCodeToSaveInLuna8266();
  tcpCreate(ip, port);
  console.debug("enter sendCode()");
}

function tcpDisconnect() {
  tcp.disconnect(createInfo.socketId, () => {
    console.debug("close socket");
  });
}

async function getIPofLuna8266ModeSTA(
  ip: string,
  port: number,
  code: string
) {
  codeTosend.setCode(code);
  tcpCreate(ip, port);
  console.debug("enter sendCode()");
}

async function tcpCreate(ip: string, port: number) {
  await tcp.create({}, (thisCreateInfo: chrome.sockets.CreateInfo) => {
    createInfo = thisCreateInfo;
    if (createInfo != undefined && createInfo.socketId >= 0) {
      console.debug("Sockect creado con id: " + createInfo.socketId);
      statusTCPConnect = StatusTask.INPROCESS;
      tcpConnect(ip, port);
    } else {
      statusTCPConnect = StatusTask.ERROR;
      debug("No fue posible crear el ID para el socket");
      return false;
    }
  });
}

async function tcpConnect(ip: string, port: number) {
  tcp.connect(createInfo.socketId, ip, port, (resultConnect: number) => {
    sockectLuna = resultConnect;
    debug("statusConnect: " + sockectLuna);
    if (sockectLuna >= 0) {
      toastMsg({
        data: "Ha sido posible realizar la conexión :" + ip + ":" + port,
        logs: ["debug"],
      });
      statusTCPConnect = StatusTask.INPROCESS;
      debug(sockectLuna);
      msgReceivedCounter = 0;
    } else {
      debug(
        "Se ha generado un error en la conexión ip: " +
          ip +
          " error: " +
          sockectLuna
      );
      statusTCPConnect = StatusTask.ERROR;
    }
  });
}

export async function tcpClose() {
  tcp.close(createInfo.socketId, () => {
    debug("close socket");
    statusTCPConnect = StatusTask.SUCCESS;
  });
}

// -- START Send and received TCP --
async function tcpSendStr(code: string) {
  const buf = await str2ab(code);
  tcp.send(createInfo.socketId, buf, (sendInfo: chrome.sockets.SendInfo) => {
    // console.debug("sendInfo: ");
    // console.debug(sendInfo);
    if (codeTosend.getLenSplitCode() == msgReceivedCounter + 1) {
      toastMsg({
        data: "Se ha enviado el programa al esp8266.",
        type: "success",
        time: 3000,
      });
      statusTCPConnect = StatusTask.INPROCESS;
      tcpClose();
    }
  });
}

tcp.onReceive.addListener(async (receivedInfo: chrome.sockets.ReceiveInfo) => {
  if (msgReceivedCounter == 0) {
    const temp = await ab2str(receivedInfo.data);
    ipStaEsp8266 =
      temp.split(",")[1] != undefined
        ? temp.split(",")[1].replace(" \r\n", "")
        : "";
    ssidOfesp8266 =
      temp.split(",")[2] != undefined
        ? temp.split(",")[1].replace("\r", "").replace("\n", "")
        : "";
    debug("ipStaEsp8266: " + ipStaEsp8266);
    debug("ssidOfesp8266: " + ssidOfesp8266);
    debug("numero de datos a enviar: " + codeTosend.getLenSplitCode());
    debug(codeTosend.getCodeFromArray(msgReceivedCounter));
    tcpSendStr(codeTosend.getCodeFromArray(msgReceivedCounter));
  } else {
    msgReceived = await ab2str(receivedInfo.data);
    console.debug(msgReceived);
    console.debug(codeTosend.getCodeFromArray(msgReceivedCounter));
    tcpSendStr(codeTosend.getCodeFromArray(msgReceivedCounter));
  }
  msgReceivedCounter += 1;
});

// https://developer.chrome.com/blog/how-to-convert-arraybuffer-to-and-from-string/
async function str2ab(str: string) {
  const buf = new ArrayBuffer(str.length); // 2 bytes for each char
  const bufView = new Uint8Array(buf);
  for (let i = 0, strLen = str.length; i < strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}

// https://developer.chrome.com/blog/how-to-convert-arraybuffer-to-and-from-string/
async function ab2str(buf: ArrayBuffer) {
  return await String.fromCharCode.apply(null, new Uint8Array(buf));
}

tcp.onReceiveError.addListener((receivedErrorInfo: chrome.sockets.ReceiveErrorInfo) => {
  debug(receivedErrorInfo);
  // statusTCPConnect = StatusTask.ERROR;
});
// -- END Send and received TCP --
