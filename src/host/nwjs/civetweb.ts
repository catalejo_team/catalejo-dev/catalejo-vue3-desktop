import { log, debug, toastMsg } from "../general/logInfo";
import { handleChatLog } from "../general/chat";
import { ab2str } from "../general/convertTypeData";
import { WebserviceStatus } from "../general/webservice"

/**
 * platform Se hace una consulta a nw sobre el s.o donde se está ejecutando
 * esta aplicación.
 */
const platform = nw.process.platform;

/**
 * Dependiendo de la plataforma el comando chuck tiene un path diferente
 */
const civetWebExec = platform == "win32" ? "civetweb\\CivetWeb32.exe" : "civetweb/civetweb";

// -- START CHUCK EJECUTAR Y DETENER --

const spawn = nw.require("child_process").spawn; // Manejo de procesos hijos
let children = []; // Lista de procesos hijos

let civetwebState = WebserviceStatus.STOP;

export function getCivetwebState() {
  return civetwebState;
}

/**
 * @function startChuck
 * Se inicia chuck de manera local y se guarda una referencia de esté
 * proceso hijo en children.
 */
export async function startCivetweb(): Promise<string | undefined> {
  const child = await spawn(civetWebExec, ["-document_root", "../dist"]);
  children.push(child);
  debug("init civetweb");
  civetwebState = WebserviceStatus.START;
  return new Promise((resolveFunc) => {
    child.stdout.on("data", async (data: ArrayBuffer) => {
      log(await ab2str(data));
    });
    child.stderr.on("data", async (data: ArrayBuffer) => {
      const message = await ab2str(data);
      log("civetweb.stderr: " + message);
      handleChatLog(message);
    });
    child.on("close", (data: string) => {
      log("civetweb.close: " + data);
      civetwebState = WebserviceStatus.STOP;
      resolveFunc(data);
    });
  });
}

/**
 * @function closeChildren
 * Se solicita cerrar todos los subprocesos inicados con la aplicación
 * y entre ellos si está iniciado chuck se detendrá este proceso.
 */
export async function closeChildren() {
  console.log("killing", children.length, "child processes");
  children.forEach((child) => {
    try {
      process.kill(child.pid);
    } catch (e) {
      console.log(e);
    }
  });
}

/**
 * Si se solicita el cierre de la aplicación, se cerrará todos los subprocesos
 * asociados que se encuentren en la lista children
 */
nw.Window.get().on("close", async () => {
  await closeChildren();
  process.exit();
});
// -- END CHUCK EJECUTAR Y DETENER


