import { ab2str } from "../general/convertTypeData";
import { log, debug, toastMsg } from "../general/logInfo";
import { ChuckStatus } from "../general/chuck";
import { handleChatLog } from "../general/chat";

/**
 * platform Se hace una consulta a nw sobre el s.o donde se está ejecutando
 * esta aplicación.
 */
const platform = nw.process.platform;

/**
 * Dependiendo de la plataforma el comando chuck tiene un path diferente
 */
const chuckExec = platform == "win32" ? "chuck\\chuck" : "chuck/chuck";

// -- START CHUCK EJECUTAR Y DETENER --

const spawn = nw.require("child_process").spawn; // Manejo de procesos hijos
let children = []; // Lista de procesos hijos

/**
 * chuckOn contiene información sobre el estado de chuck local, si se ha
 * solicitado o no el lanzamiento de chuck en este equipo.
 */
let chuckOn = ChuckStatus.DISCONNECT;
export function getStatusChuck() {
  return chuckOn;
}

// TODO: start chuck remote?
// tener en cuenta la variable chuckOn la cual puede
// desaparecer
/**
 * @function startChuck
 * Se inicia chuck de manera local y se guarda una referencia de esté
 * proceso hijo en children.
 */
export async function startChuck(): Promise<string | undefined> {
  if (chuckOn == ChuckStatus.DISCONNECT) {
    const child = await spawn(chuckExec, ["--loop", "--port:2215"]);
    children.push(child);
    return new Promise((resolveFunc) => {
      chuckOn = ChuckStatus.LOCAL_CONNECT;
      child.stdout.on("data", async (data: ArrayBuffer) => {
        log(await ab2str(data));
      });
      child.stderr.on("data", async (data: ArrayBuffer) => {
        const message = await ab2str(data);
        log("startChuck.stderr: " + message);
        handleChatLog(message);
      });
      child.on("close", (data: string) => {
        log("startChuck.close: " + data);
        resolveFunc(data);
      });
    });
  }
}

/**
 * @function closeChildren
 * Se solicita cerrar todos los subprocesos inicados con la aplicación
 * y entre ellos si está iniciado chuck se detendrá este proceso.
 */
export async function closeChildren() {
  console.log("killing", children.length, "child processes");
  children.forEach((child) => {
    try {
      process.kill(child.pid);
    } catch (e) {
      console.log(e);
    }
  });
}

/**
 * Si se solicita el cierre de la aplicación, se cerrará todos los subprocesos
 * asociados que se encuentren en la lista children
 */
nw.Window.get().on("close", async () => {
  await closeChildren();
  process.exit();
});
// -- END CHUCK EJECUTAR Y DETENER

// -- START CHUCK OPTIONS --
let optionsChuck: Array<string>;
optionsChuck = [];
export function setOptionChuck(newOptions: Array<string>) {
  optionsChuck = newOptions;
  return optionsChuck;
}
// -- END CHUCK OPTIONS --

// -- START COMANDOS CHUCK --
export async function killChuck(): Promise<string> {
  const ret = await cmd(chuckExec, ["--kill"]);
  chuckOn = ChuckStatus.DISCONNECT;
  return ret;
}

export async function removeTheLastScripChuck(): Promise<string> {
  const ret = await cmd(chuckExec, ["--"]);
  return ret;
}

export async function removeScriptChuck(id: string): Promise<string> {
  const ret = await cmd(chuckExec, ["-", id]);
  return ret;
}

export async function removeAllScriptChuck(): Promise<string> {
  const ret = await cmd(chuckExec, ["--remove.all"]);
  return ret;
}

export async function addScriptChuck(file: string): Promise<string> {
  const ret = await cmd(chuckExec, ["+", file]);
  return ret;
}

export function cmd(program: string, argument: Array<string>): Promise<string> {
  const child = spawn(program, [...optionsChuck, ...argument]);
  return new Promise((resolveFunc) => {
    child.stdout.on("data", async (data: ArrayBuffer) => {
      log(await ab2str(data));
    });
    child.stderr.on("data", async (data: ArrayBuffer) => {
      log(await ab2str(data));
    });
    child.on("close", (data: string) => {
      log("close: " + data);
      resolveFunc(data);
    });
  });
}
// -- END COMANDOS CHUCK --

// -- START MANEJO DE ARCHIVOS CHUCK --
export async function getPathChuckFile(
  fileHandle: File,
  typeEditor: number,
  position: number
) {
  let path = fileHandle.path;
  if (platform == "win32") {
    path = path.replaceAll("\\", "/");
    toastMsg({
      data: path,
      time: 5000,
    });
  }
  return await (path + "." + position + "." + typeEditor + ".ck");
}

import { writeFile } from "./fileManager";

export async function saveChuckFile(path: string, content: string) {
  try {
    await writeFile(path, content);
    return "Archivo guardado";
  } catch (err) {
    return "Error al tratar de guardar en el archivo";
  }
}
// -- END MANEJO DE ARCHIVOS CHUCK --

// -- START CONSULTA DE SERVICIOS CHUCK EN LA RED  POR IP --

export class ChuckConnectIP {
  /* IP válidas que pueden contener un servicio de chuck remoto*/
  valideChuckIpList: Array<string>;
  /* hostChuckIp IP de este equipo */
  hostChuckIp: string;
  /* IP de un servicio de chuck remoto seleccionado */
  ipChuckRemote: string;
  /* Primeros 3 octetos de IP host, ejemplo 192.168. */
  hostChuckIpUp: string;
  /* countSearchChuckIP contador para búsqueda de ip, este buscador
   * corresponde al último octeto y no podrá superar el valor de 256 */
  countSearchChuckIP: number;

  constructor() {
    this.valideChuckIpList = [];
    this.hostChuckIp = "";
    this.hostChuckIpUp = "";
    this.ipChuckRemote = "";
    this.countSearchChuckIP = 0;
  }
  setCountSearchChuckIP(num: number) {
    this.countSearchChuckIP = num;
  }
  setIpChuckRemote(ip: string) {
    this.ipChuckRemote = ip;
  }
  getIpChuckRemote() {
    return this.ipChuckRemote;
  }
  /* setIpHost() es un método que obtiene la IP del host y además pone
   * hostChuckIpUp pone los 3 octetos */
  setIpHost() {
    const networkInterfaces = nw.require("os").networkInterfaces();
    for (const key in networkInterfaces) {
      if (key != "lo") {
        const listIpOfInterface = networkInterfaces[key];
        for (const element in listIpOfInterface) {
          if (listIpOfInterface[element].family == "IPv4") {
            // configurando IP del Host
            this.hostChuckIp = listIpOfInterface[element].address;
            // configura los 3 octetos
            this.setIpUpHost();
            return true;
          }
        }
        return false;
      }
    }
  }
  getIpHost() {
    return this.hostChuckIp;
  }
  setIpUpHost() {
    const ipSelectSplit = this.hostChuckIp.split(".");
    let ipUp = "";
    for (let i = 0, len = ipSelectSplit.length; i + 1 < len; i++) {
      ipUp = ipUp + ipSelectSplit[i] + ".";
    }
    this.hostChuckIpUp = ipUp;
    return;
  }
  getIpUpHost() {
    return this.hostChuckIpUp;
  }
  /* Busca IP que contengan chuck y si es así enlista las IP */
  async scanChuckServiceByIp() {
    this.setIpHost();
    this.valideChuckIpList = [];
    this.countSearchChuckIP = 0;
    for (
      this.countSearchChuckIP;
      this.countSearchChuckIP < 256;
      this.countSearchChuckIP++
    ) {
      const ip = this.getIpUpHost() + this.countSearchChuckIP;
      const ret = await cmd(chuckExec, [
        "--remote:" + ip,
        "--port:2215",
        "--status",
      ]);
      toastMsg({
        data: "Scaneando chuck en ip: " + ip,
        time: 1000,
      });
      if (ret == "0") {
        toastMsg({
          data: "Se ha encontrado un servicio chuck en ip: " + ip,
          type: "success",
          time: 5000,
        });
        this.valideChuckIpList.push(ip);
      }
    }
    debug(this.valideChuckIpList);
    return;
  }
  getValidadeChuckIpList() {
    return this.valideChuckIpList;
  }
}
// -- END CONSULTA DE SERVICIOS CHUCK EN LA RED  POR IP --

/**
 * @function checkChuckRemoteConection
 * Esta función comprueba que en la IP especificada haya un
 * servicio chuck remoto y si es así realiza una configuración
 * en las opciones del comando cmd() responsable de las tareas
 * de chuck
 */
export async function checkChuckRemoteConection(ip: string) {
  const ret = await cmd(chuckExec, [
    "--remote:" + ip,
    "--port:2215",
    "--status",
  ]);
  if (ret == "0") {
    setOptionChuck(["--remote:" + ip, "--port:2215"]);
    return true;
  }
  return false;
}
