import { str2ab, ab2str } from "../general/convertTypeData";
import { toastMsg, StatusTask } from "../general/logInfo";

export let statusUSBConnect: StatusTask; // Seguimiento de la conexión ip con el esp8266

const platform = nw.process.platform;

// Copy of conecction id for implict operations
export let idConnectionSerial: number;

function selectSerialPortListEnabled(portList: Array<object>) {
  const porListEnabled: Array<object> = [];
  if (platform == "win32") {
    for (const port in portList) {
      if (portList[port].productId != 0) {
        if (portList[port].path.startsWith("COM")) {
          porListEnabled.push(portList[port]);
        }
      }
    }
    return porListEnabled;
  } else {
    for (const port in portList) {
      if (portList[port].productId != 0) {
        if (
          portList[port].path.startsWith("/dev/ttyUSB") ||
          portList[port].path.startsWith("/dev/ttyACM")
        ) {
          porListEnabled.push(portList[port]);
        }
      }
    }
    return porListEnabled;
  }
}

/**
 * @function getSerialPortList
 *
 */
export async function getSerialPortList(): Promise<object> {
  return new Promise((resolve) => {
    chrome.serial.getDevices((portList: Array<object>) => {
      resolve(selectSerialPortListEnabled(portList));
    });
  });
}

/**
 * @function connect2SerialDevice
 * @param {string} path - Path representativo del dispositivo serial.
 *
 */
export async function connect2SerialDevice(path: string): Promise<number> {
  return new Promise((resolve) => {
    chrome.serial.connect(
      path,
      { bitrate: 115200 },
      (connectionInfo: object) => {
        idConnectionSerial = connectionInfo.connectionId;
        resolve(idConnectionSerial);
      },
    );
  });
}

export function disconnect() {
  return new Promise((resolve) => {
    chrome.serial.disconnect(idConnectionSerial, (result: object) => {
      if (result) {
        resolve(0);
      } else {
        resolve(-1);
      }
    });
  });
}

/**
 * @function sendFromSerialPort
 *
 */
export async function sendFromSerialPort(msg: ArrayBuffer) {
  return new Promise((resolve) => {
    chrome.serial.send(idConnectionSerial, msg, (onSend) => {
      console.log(onSend);
      resolve(1);
    });
  });
}

export let msgRecSerial = "";

const onReceiveCallback = (info: object) => {
  if (info.connectionId == idConnectionSerial && info.data) {
    const str = ab2str(info.data);
    if (str.charAt(str.length - 1) === "\n") {
      msgRecSerial += str.substring(0, str.length - 1);
      // onLineReceived(msgRecSerial);
      msgRecSerial = "";
    } else {
      msgRecSerial += str;
    }
    console.log(msgRecSerial);
  }
};

export function receivedMsg(onReceiveCallback: object) {
  chrome.serial.onReceive.addListener(onReceiveCallback);
}

export async function test() {
  console.log(await getSerialPortList());
  idConnectionSerial = await connect2SerialDevice("/de/ttyUSB0");
  sendFromSerialPort(str2ab("mensage dos"));
  receivedMsg();

  // console.log("hola");
  // await configSerialDevice("/dev/ttyUSB0");
  // await serialDevice.onOpen((error) => {
  //   if (error) {
  //     console.log(error)
  //   } else {
  //     serialDevice.write("hola");
  //   }
  // });
}

let codeLuaToSend: Array<string>;
let codeLuaToSendCount: number;

export async function saveCodeInLuna8266FromUSB(code: Array<string>) {
  codeLuaToSendCount = 0;
  receivedMsg(luna8266ReceiveCB);
  codeLuaToSend = code;
  await sendFromSerialPort(str2ab(codeLuaToSend[0]));
  codeLuaToSendCount++;
}

const luna8266ReceiveCB = async (info: object) => {
  if (info.connectionId == idConnectionSerial && info.data) {
    const str = ab2str(info.data);
    // if (str.charAt(str.length - 1) === "\n") {
    if (
      str.substr(str.length -2) === "> " ||
      str.substr(str.length -3) === ">> "
    ) {
      msgRecSerial += str.substring(0, str.length - 1);
      // onLineReceived(msgRecSerial);
      msgRecSerial = "";
      if (codeLuaToSendCount < codeLuaToSend.length) {
        console.log(msgRecSerial);
        await sendFromSerialPort(str2ab(codeLuaToSend[codeLuaToSendCount]));
        codeLuaToSendCount++;
      } else if (codeLuaToSendCount == codeLuaToSend.length) {

      }
    } else {
      msgRecSerial += str;
    }
  }
};
