import { toastMsg, debug, StatusTask } from "../general/logInfo";
import { handleChatLog } from "../general/chat";

export let statusUSBConnect: StatusTask; // Seguimiento de la conexión ip con el esp8266

export async function saveCodeInLuna8266(code: string) {
  statusUSBConnect = StatusTask.INPROCESS;
  codeTosend.setCode(code);
  codeTosend.setCodeToSaveInLuna8266();
  saveCodeInLuna8266FromUSB(codeTosend.getCodeToSendUSB());
}

export async function testUSBConnect(path: string) {
  statusUSBConnect = StatusTask.INPROCESS;
  const state = await connect2SerialDevice(path);
  if (state >= 0) {
    statusUSBConnect = StatusTask.SUCCESS;
  } else {
    statusUSBConnect = StatusTask.ERROR;
  }
  return;
}

export async function usbDisconnect() {
  return await disconnect();
}

export function getStatusConnectLunaUSB() {
  debug(statusUSBConnect);
  return statusUSBConnect;
}

class CodeToSend {
  code: string;
  splitCode: Array<string>;
  lenSplitCode: number;
  constructor() {
    this.code = "";
    this.splitCode = [];
    this.lenSplitCode = 0;
  }
  setCode(code: string) {
    this.code = code;
    this.splitCode = code.split("\n");
    this.lenSplitCode = this.splitCode.length;
  }
  getLenSplitCode() {
    return this.lenSplitCode;
  }
  getCodeFromArray(index: number) {
    if (index < this.lenSplitCode) {
      return this.splitCode[index] + "\n";
    }
    return "undefined";
  }
  getSplitCode() {
    return this.splitCode;
  }
  setCodeToSaveInLuna8266() {
    let tempCode = "";
    for (const indexCode in this.splitCode) {
      tempCode += "save__data" + this.splitCode[indexCode] + "\n";
    }
    tempCode = "save__begin\n" + tempCode + "save__end\n";
    // tempCode = "save__begin\n" + tempCode + "save__end\nsave__rs\n";
    this.splitCode = tempCode.split("\n");
    this.lenSplitCode = this.splitCode.length;
  }
  getCodeToSendUSB() {
    const codeLuaToSave: Array<string> = [];
    codeLuaToSave[0] =
      "uart.setup(0,115200,8,uart.PARITY_NONE,uart.STOPBITS_1,0)\n";
    codeLuaToSave.push("function usbSave(data)\n");
    codeLuaToSave.push("gpio.mode(4,gpio.OUTPUT)\n");
    codeLuaToSave.push("gpio.mode(4,gpio.LOW)\n");
    codeLuaToSave.push("saveLua(data)\n");
    codeLuaToSave.push("end\n");
    for (const indexCode in this.splitCode) {
      codeLuaToSave.push('usbSave("' + this.splitCode[indexCode] + '\\n")\n');
    }
    return codeLuaToSave;
  }
}

const codeTosend = new CodeToSend();

//
// SERIAL PORT

import { str2ab, ab2str } from "../general/convertTypeData";


const platform = nw.process.platform;

// Copy of conecction id for implict operations
export let idConnectionSerial: number;

function selectSerialPortListEnabled(portList: Array<object>) {
  const porListEnabled: Array<object> = [];
  if (platform == "win32") {
    for (const port in portList) {
      if (portList[port].productId != 0) {
        if (portList[port].path.startsWith("COM")) {
          porListEnabled.push(portList[port]);
        }
      }
    }
    return porListEnabled;
  } else {
    for (const port in portList) {
      if (portList[port].productId != 0) {
        if (
          portList[port].path.startsWith("/dev/ttyUSB") ||
          portList[port].path.startsWith("/dev/ttyACM")
        ) {
          porListEnabled.push(portList[port]);
        }
      }
    }
    return porListEnabled;
  }
}

/**
 * @function getSerialPortList
 *
 */
export async function getSerialPortList(): Promise<object> {
  return new Promise((resolve) => {
    chrome.serial.getDevices((portList: Array<object>) => {
      resolve(selectSerialPortListEnabled(portList));
    });
  });
}

export async function connect2SerialDevice(path) {
  const port = await chrome.serial.connect(path, {bitrate: 115200}, () => {});
  console.log(port);
  return port
}

/**
 * @function connect2SerialDevice
 * @param {string} path - Path representativo del dispositivo serial.
 *
 */
export async function connect2SerialDevice1(path: string): Promise<number> {
  return new Promise((resolve, reject) => {
    chrome.serial.connect(
      path,
      { bitrate: 115200 },
      (connectionInfo: object) => {
        if (chrome.runtime.lastError) {
          reject(chrome.runtime.lastError);
        } else {
          idConnectionSerial = connectionInfo.connectionId;
          resolve(idConnectionSerial);
        }
      },
    );
  });
}

export function disconnect() {
  return new Promise((resolve) => {
    chrome.serial.disconnect(idConnectionSerial, (result: object) => {
      if (result) {
        resolve(0);
      } else {
        resolve(-1);
      }
    });
  });
}

/**
 * @function sendFromSerialPort
 *
 */
export async function sendFromSerialPort(msg: ArrayBuffer) {
  return new Promise((resolve) => {
    chrome.serial.send(idConnectionSerial, msg, (onSend) => {
      handleChatLog(ab2str(msg));
      console.log(onSend);
      resolve(1);
    });
  });
}

export let msgRecSerial = "";

const onReceiveCallback = (info: object) => {
  if (info.connectionId == idConnectionSerial && info.data) {
    const str = ab2str(info.data);
    if (str.charAt(str.length - 1) === "\n") {
      msgRecSerial += str.substring(0, str.length - 1);
      // onLineReceived(msgRecSerial);
      msgRecSerial = "";
    } else {
      msgRecSerial += str;
    }
    console.log(msgRecSerial);
  }
};

export function receivedMsg(onReceiveCallback: object) {
  chrome.serial.onReceive.addListener(onReceiveCallback);
}

let codeLuaToSend: Array<string>;
let codeLuaToSendCount: number;

export async function saveCodeInLuna8266FromUSB(code: Array<string>) {
  codeLuaToSendCount = 0;
  receivedMsg(luna8266ReceiveCB);
  codeLuaToSend = code;
  await sendFromSerialPort(str2ab(codeLuaToSend[0]));
  codeLuaToSendCount++;
}

const luna8266ReceiveCB = async (info: object) => {
  if (info.connectionId == idConnectionSerial && info.data) {
    const str = ab2str(info.data);
    // if (str.charAt(str.length - 1) === "\n") {
    if (
      str.substr(str.length -2) === "> " ||
      str.substr(str.length -3) === ">> "
    ) {
      msgRecSerial += str.substring(0, str.length - 1);
      // onLineReceived(msgRecSerial);
      msgRecSerial = "";
      if (codeLuaToSendCount < codeLuaToSend.length) {
        console.log(msgRecSerial);
        await sendFromSerialPort(str2ab(codeLuaToSend[codeLuaToSendCount]));
        codeLuaToSendCount++;
      } else if (codeLuaToSendCount == codeLuaToSend.length) {
        statusUSBConnect = StatusTask.SUCCESS;
        toastMsg({
          data: "Programa enviado a la tarjeta",
          type: "success",
          time: 2000,
        });
      }
    } else {
      msgRecSerial += str;
    }
  }
};
