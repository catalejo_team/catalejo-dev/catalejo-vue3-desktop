import { toastMsg, debug, error } from "../general/logInfo";
const fileSystem = nw.require("fs");
const fileSystemPromises = nw.require("fs").promises;

// TODO: por ahora sin uso, se recomienda no borrarla
export async function getPathAndNameOfFile(fileHandle: File) {
  let splitPath: Array<string>;
  let pwd: string;
  if (process.platform == "linux") {
    splitPath = fileHandle.path.split("/");
    pwd = "";
    for (let i = 0, len = splitPath.length; i < len - 1; i++) {
      pwd += splitPath[i] + "/";
    }
    return {
      pwd,
      name: fileHandle.name,
    };
  } else if (process.platform == "windows") {
    splitPath = fileHandle.path.split("\\");
    pwd = "";
    for (let i = 0, len = splitPath.length; i < len - 1; i++) {
      pwd += splitPath[i] + "\\";
    }
    return {
      pwd,
      name: fileHandle.name,
    };
  }
}

export async function writeFile(pathFile: string, content: string) {
  try {
    await fileSystemPromises.writeFile(pathFile, content);
    return "Archivo guardado";
  } catch (err) {
    return "Error al tratar de guardar en el archivo";
  }
}

import { CatalejoProjects, CatalejoDataPrj } from "../defCatalejo";

export async function getFileHandle(file: File) {
  let log: string;

  const stringFile = await file.text();
  try {
    const catalejoProjectObj = await JSON.parse(stringFile); // TODO se arreglará cuando se esté guardando los proyectos en .txt o .json
    if (catalejoProjectObj.TYPE_PRJ != undefined) {
      for (const typePrj in CatalejoProjects) {
        if (catalejoProjectObj.TYPE_PRJ == CatalejoProjects[typePrj].name) {
          log = "Loading project";
          console.debug(log);
          return { file, log };
        }
      }
    } else {
      log = "El archivo no contiene un proyecto válido";
      console.debug(log);
      return { undefined, log };
    }
  } catch (e) {
    log = "El archivo cargado no es un proyecto catalejo";
    console.log(e);
    return { undefined, log };
  }
}

export async function saveCatalejoPrj(
  fileHandle: object,
  catalejoPrj: CatalejoDataPrj
) {
  try {
    const contents = await JSON.stringify(catalejoPrj.getCatalejoData());
    try {
      const result = await writeFile(fileHandle.path, contents);
      toastMsg({
        data: "Cambios guardados en el archivo: " + fileHandle.path,
        type: "success"
      });
      debug(result);
    } catch (e) {
      toastMsg({
        data: "Se produjo un error al intentar guardar en el archivo",
        type: "error",
      });
      error(e);
    }
  } catch (e) {
    error(e);
  }
}

export async function updateEditorAndBlockly(fileHandle: object) {
  const stringFile = await fileHandle.text();
  const catalejoProjectObj = await JSON.parse(stringFile);
  for (const typePrj in CatalejoProjects) {
    if (catalejoProjectObj.TYPE_PRJ == CatalejoProjects[typePrj].name) {
      return {
        optionsBlockly: CatalejoProjects[typePrj].optionsBlockly,
        catalejoProjectObj: catalejoProjectObj,
      };
    }
  }
  return null;
}

// TODO: no está en uso
export async function getNewFileHandle() {
  // const [newFileHandle] = await window.showSaveFilePicker(compatibleTypeFileCatalejo);
  const newFileHandle = "";
  console.debug(newFileHandle);
  return newFileHandle;
}
