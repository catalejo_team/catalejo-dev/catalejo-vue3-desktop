# Referencias de submodulos traidos del proyecto blockly

Algunos módulos son agregados directamente del proyecto de blockly,
lo cual para efecto de mantenimiento requerá actualizaciones desde archivos fuente

## Modulos fuente

* [**modal**](https://github.com/google/blockly-samples/tree/master/plugins/modal)
* [**typed-variable-modal**](https://github.com/google/blockly-samples/tree/master/plugins/typed-variable-modal)


