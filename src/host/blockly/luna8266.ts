import { luaGenerator } from "../../blockly-config/luna8266/luna8266";

export async function getCodeLuaFromBlockly(workspace: object) {
  luaGenerator.INFINITE_LOOP_TRAP = null // eslint-disable-line
  return await luaGenerator.workspaceToCode(workspace) // eslint-disable-line
}
