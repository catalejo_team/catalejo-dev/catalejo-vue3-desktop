import * as Blockly from "@johnnycubides/blockly-catalejo";

export function setBlocklyOption(toolbox: string, theme: Blockly.Theme) {
  return {
    media: "media/",
    theme: theme, // DarkTheme funciona con blockly 9.0 en adelante
    renderer: "geras", // geras, thrasos, zelos
    toolboxPosition: "start", // [toolboxPosition, horizontalLayout] top: [start, true], bottom:end-true, left: [start, false], right:[end, false]
    horizontalLayout: false,
    zoom: {
      controls: true,
      wheel: true,
      startScale: 1.0,
      maxScale: 3,
      minScale: 0.3,
      scaleSpeed: 1.2,
      pinch: true,
    },
    trashcan: true,
    grid: {
      spacing: 25,
      length: 1,
      colour: "#ccc",
      snap: true,
    },
    toolbox: toolbox,
  };
}

const componentStylesDark = {
  'workspaceBackgroundColour': '#1e1e1e',
  'toolboxBackgroundColour': 'blackBackground',
  'toolboxForegroundColour': '#fff',
  'flyoutBackgroundColour': '#252526',
  'flyoutForegroundColour': '#ccc',
  'flyoutOpacity': 1,
  'scrollbarColour': '#797979',
  'insertionMarkerColour': '#fff',
  'insertionMarkerOpacity': 0.3,
  'scrollbarOpacity': 0.4,
  'cursorColour': '#d0d0d0',
  'blackBackground': '#333',
};

const darkItheme = {
  "base": Blockly.Themes.Classic,
  "name": "DarkTheme",
  "componentStyles": componentStylesDark,
};

export const DarkTheme = Blockly.Theme.defineTheme("DarkTheme", darkItheme);

export async function getXMLFromBlockly(workspace: Blockly.Workspace) {
  return await Blockly.Xml.workspaceToDom(workspace);
}

// export async function getCodeFromBlockly(workspace: Blockly.Workspace) {
// }
