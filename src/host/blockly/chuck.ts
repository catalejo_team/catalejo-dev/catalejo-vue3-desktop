import * as Blockly from "@johnnycubides/blockly-catalejo";
import { TypedVariableModal } from "./type-variable-modal/index";
import { chuckGenerator } from "../../blockly-config/chuck/chuck";

export async function getCodeChuckFromBlockly(workspace: Blockly.Workspace) {
  chuckGenerator.INFINITE_LOOP_TRAP = null // eslint-disable-line
  return await chuckGenerator.workspaceToCode(workspace) // eslint-disable-line
}

const createFlyout = function (workspace: Blockly.Workspace) {
  let xmlList = [];
  // Add your button and give it a callback name.
  const button = document.createElement('button');
  button.setAttribute('text', 'Create Typed Variable');
  button.setAttribute('callbackKey', 'callbackName');

  xmlList.push(button);

  // This gets all the variables that the user creates and adds them to the
  // flyout.
  const blockList = Blockly.VariablesDynamic.flyoutCategoryBlocks(workspace);
  xmlList = xmlList.concat(blockList);
  return xmlList;
};

const createFlyout1 = function (workspace: Blockly.Workspace) {
  let xmlList1 = [];
  // Add your button and give it a callback name.
  const button = document.createElement('button');
  button.setAttribute('text', 'Create Oscilador');
  button.setAttribute('callbackKey', 'callbackName1');

  xmlList1.push(button);

  // This gets all the variables that the user creates and adds them to the
  // flyout.
  const blockList1 = Blockly.VariablesDynamic.flyoutCategoryBlocks(workspace);
  xmlList1 = xmlList1.concat(blockList1);
  return xmlList1;
};

const createFlyoutInstrument = function (workspace: Blockly.Workspace) {
  let xmlListInstrument = [];
  // Add your button and give it a callback name.
  const button = document.createElement('button');
  button.setAttribute('text', 'Crear instrumento');
  button.setAttribute('callbackKey', 'callbackNameInstrument');

  xmlListInstrument.push(button);

  // This gets all the variables that the user creates and adds them to the
  // flyout.
  const blockListInstrument = Blockly.VariablesDynamic.flyoutCategoryBlocks(workspace);
  xmlListInstrument = xmlListInstrument.concat(blockListInstrument);
  return xmlListInstrument;
};

const createFlyoutCommunication = function (workspace: Blockly.Workspace) {
  let xmlListCommunication = [];
  // Add your button and give it a callback name.
  const button = document.createElement('button');
  button.setAttribute('text', 'Crear comunicación');
  button.setAttribute('callbackKey', 'callbackNameCommunication');

  xmlListCommunication.push(button);

  // This gets all the variables that the user creates and adds them to the
  // flyout.
  const blockListCommunication = Blockly.VariablesDynamic.flyoutCategoryBlocks(workspace);
  xmlListCommunication = xmlListCommunication.concat(blockListCommunication);
  return xmlListCommunication;
};

let typedVarModal;
let typedVarModal1;
let typedVarModalInstrument;
let typedVarModalCommunication;

export async function updateTypedVarModalForChuck(workspace: Blockly.Workspace) {
  await workspace.registerToolboxCategoryCallback(
    'CREATE_TYPED_VARIABLE',
    createFlyout
  );

  await workspace.registerToolboxCategoryCallback(
    'CREATE_TYPED_VARIABLE1',
    createFlyout1
  );

  await workspace.registerToolboxCategoryCallback(
    'CREATE_TYPED_VARIABLE_INSTRUMENT',
    createFlyoutInstrument
  );

  await workspace.registerToolboxCategoryCallback(
    'CREATE_TYPED_VARIABLE_COMMUNICATION',
    createFlyoutCommunication
  );

  typedVarModal = await new TypedVariableModal(
    workspace, 'callbackName',
    [
      ["Entero", "int"],
      ["Flotante", "float"],
      ["Texto", "string"],
      ["Duración", "dur"],
      ["Lista", "array"]
  ]);

  typedVarModal1 = await new TypedVariableModal(
    workspace, 'callbackName1',
    [
      ["Oscilador Seno", "SinOsc"],
      ["Oscilador Triangular", "TriOsc"],
      ["Oscilador de pulso", "PulseOsc"],
      ["Oscilador de sierra", "SawOsc"],
      ["Oscilador cuadrado", "SqrOsc"],
  ]);

  typedVarModalInstrument = await new TypedVariableModal(
    workspace, 'callbackNameInstrument',
    [
      ["Piano", "Rhodey"],
      ["Clarinete", "Clarinet"],
      ["Archivo", "SndBuf"],
  ]);

  typedVarModalCommunication = await new TypedVariableModal(
    workspace, 'callbackNameCommunication',
    [
      ["OSC enviar", "OscOut"],
      ["OSC recibir", "OscIn"],
      ["OSC mensaje", "OscMsg"],
      ["MIDI enviar", "MidiOut"],
      ["MIDI recibir", "MidiIn"],
      ["MIDI mensaje", "MidiMsg"],
      ["Serial", "SerialIO"],
  ]);

  typedVarModal.init();
  typedVarModal1.init();
  typedVarModalInstrument.init();
  typedVarModalCommunication.init();
}
