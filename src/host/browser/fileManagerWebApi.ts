import { CatalejoDataPrj, CatalejoProjects } from "../defCatalejo";
import { error, terminal } from "../general/logInfo";

export async function saveCatalejoPrj(
  fileHandle: object,
  catalejoPrj: CatalejoDataPrj
) {
  try {
    const contents = await JSON.stringify(catalejoPrj.getCatalejoData());
    try {
      // tslint:disable-next-line
      const writable = await fileHandle.createWritable();
      try {
        await writable.write(contents);
        await writable.close();
        terminal("Save in file");
        return {
          status: "OK",
          log: "guardado en archivo",
        };
      } catch (e) {
        error(e);
      }
    } catch (e) {
      error(e);
    }
  } catch (e) {
    error(e);
  }
}

export async function updateEditorAndBlockly(fileHandle: object) {
  const fileToLoad = await fileHandle.getFile();
  const stringFile = await fileToLoad.text();
  const catalejoProjectObj = await JSON.parse(stringFile);
  for (const typePrj in CatalejoProjects) {
    if (catalejoProjectObj.TYPE_PRJ == CatalejoProjects[typePrj].name) {
      return {
        optionsBlockly: CatalejoProjects[typePrj].optionsBlockly,
        catalejoProjectObj: catalejoProjectObj,
      };
    }
  }
  return null;
}

import { compatibleTypeFileCatalejo } from "../defCatalejo";

export async function getFileHandle() {
  let log: string;
  try {
    const [fileHandle] = await window.showOpenFilePicker(
      compatibleTypeFileCatalejo
    );

    const file = await fileHandle.getFile();
    const stringFile = await file.text();
    try {
      const catalejoProjectObj = await JSON.parse(stringFile); // TODO se arreglará cuando se esté guardando los proyectos en .txt o .json
      if (catalejoProjectObj.TYPE_PRJ != undefined) {
        for (const typePrj in CatalejoProjects) {
          if (catalejoProjectObj.TYPE_PRJ == CatalejoProjects[typePrj].name) {
            log = "Loading project";
            terminal(log);
            return { fileHandle, log };
          }
        }
      } else {
        log = "El archivo no contiene un proyecto válido";
        terminal(log);
        return { undefined, log };
      }
    } catch (e) {
      log = "El archivo cargado no es un proyecto catalejo";
      error(e);
      return { undefined, log };
    }
  } catch (e) {
    log = "No fue posible cargar el proyecto.";
    error(e);
    return { undefined, log };
  }
}

export async function getNewFileHandle() {
  // const [newFileHandle] = await window.showSaveFilePicker(compatibleTypeFileCatalejo);
  const newFileHandle = await window.showSaveFilePicker(
    compatibleTypeFileCatalejo
  );
  terminal(newFileHandle);
  return newFileHandle;
}
