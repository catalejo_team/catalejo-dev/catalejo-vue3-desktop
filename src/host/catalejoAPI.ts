import { CatalejoDataPrj } from "./defCatalejo";

export type { CatalejoDataPrj };

import { TypeEditor } from "./defCatalejo";

/**
 * Esta función permite seleccionar un solo modo
 * del editor para que solo se pueda activar un label
 * en el Label del Editor, el cual visualiza el modo
 * del editor en el que se encuentra el área de trabajo
 */
export function selectActiveLabelOfBuffer(
  typeEditor: TypeEditor,
  blocklyBufferPos: number,
  codemirrorBufferPos: number
) {
  if (blocklyBufferPos == codemirrorBufferPos) {
    if (typeEditor == TypeEditor.Blockly) {
      return { Blockly: blocklyBufferPos, Codemirror: -1 };
    } else if (typeEditor == TypeEditor.Codemirror) {
      return { Blockly: -1, Codemirror: codemirrorBufferPos };
    }
  } else {
    return {
      Blockly: blocklyBufferPos,
      Codemirror: codemirrorBufferPos,
    };
  }
}

/**
 * función de retardo en ms para realizar pruebas por ejemplo
 * de conexión por IP
 * para usar la function se debe llamar con await sleep(ms)
 */
export function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
