// -- Select editor --
export enum TypeEditor {
  Blockly = 0,
  Codemirror = 1,
}

export enum LabelEditor {
  "Blockly", //label del buffer para blockly
  "Código", // label del buffer para codemirror
}

export enum CatalejoApp {
  Onboarding = 0,
  Menu = 1,
  Projects = 2,
  Editor = 3,
}

export type CatalejoPrj = {
  name: string;
  toolboxBlockly: string;
  optionsBlockly: object;
};

export const compatibleTypeFileCatalejo = {
  suggestedName: "untitled.txt",
  types: [
    {
      description: "Text Files",
      accept: {
        "text/plain": [".txt", ".json"],
      },
    },
  ],
};

export class CatalejoDataPrj {
  TYPE_PRJ: string;
  BLOCKLY: Array<string>;
  CODEMIRROR: Array<string>;
  constructor(
    TYPE_PRJ: string,
    BLOCKLY?: Array<string>,
    CODEMIRROR?: Array<string>
  ) {
    this.TYPE_PRJ = TYPE_PRJ;
    if (BLOCKLY === undefined) {
      const code =
        '<xml xmlns="https://developers.google.com/blockly/xml"></xml>';
      this.BLOCKLY = [
        code,
        code,
        code,
        code,
        code,
        code,
        code,
        code,
        code,
        code,
      ];
    } else {
      this.BLOCKLY = BLOCKLY;
    }
    if (CODEMIRROR === undefined) {
      this.CODEMIRROR = ["", "", "", "", "", "", "", "", "", ""];
    } else {
      this.CODEMIRROR = CODEMIRROR;
    }
  }
  getTypePrj() {
    return this.TYPE_PRJ;
  }
  getBlocklyCode(buffer: number): string {
    try {
      return this.BLOCKLY[buffer];
    } catch (e) {
      return "";
      console.debug(e);
    }
  }
  setBlocklyCode(buffer: number, BLOCKLY: string) {
    try {
      this.BLOCKLY[buffer] = BLOCKLY;
    } catch (e) {
      console.debug(e);
    }
  }
  getMirrorCode(buffer: number): string {
    try {
      return this.CODEMIRROR[buffer];
    } catch (e) {
      return "";
      console.debug(e);
    }
  }
  setMirrorCode(buffer: number, CODEMIRROR: string) {
    try {
      this.CODEMIRROR[buffer] = CODEMIRROR;
    } catch (e) {
      console.debug(e);
    }
  }
  getCatalejoData() {
    return {
      TYPE_PRJ: this.TYPE_PRJ,
      BLOCKLY: this.BLOCKLY,
      CODEMIRROR: this.CODEMIRROR,
    };
  }
  // https://betterprogramming.pub/how-to-write-an-async-class-constructor-in-typescript-javascript-7d7e8325c35e
  public static async build(
    TYPE_PRJ: string,
    BLOCKLY?: Array<string>,
    CODEMIRROR?: Array<string>
  ): Promise<CatalejoDataPrj> {
    return new CatalejoDataPrj(TYPE_PRJ, BLOCKLY, CODEMIRROR);
  }
}

import { setBlocklyOption } from "./blockly";
import { DarkTheme } from "./blockly";
//import * as DarkTheme from "@blockly/theme-dark";
import { luna8266Toolbox } from "../blockly-config/luna8266/toolbox";
import { chuckToolbox } from "../blockly-config/chuck/toolbox";

const Luna8266: CatalejoPrj = {
  name: "luna8266",
  toolboxBlockly: luna8266Toolbox,
  optionsBlockly: setBlocklyOption(luna8266Toolbox, DarkTheme),
};

const chuck: CatalejoPrj = {
  name: "chuck",
  toolboxBlockly: "", // TODO
  optionsBlockly: setBlocklyOption(chuckToolbox, DarkTheme), // TODO
};

export const CatalejoProjects: CatalejoPrj[] = [Luna8266, chuck];
export const setBlocklyOptionDefault = setBlocklyOption(
  CatalejoProjects[0].toolboxBlockly,
  DarkTheme
);
