import { createApp } from "vue";
import App from "./App.vue";

const app = createApp(App);

// -- START-- Import plugin modal vue-universal-modal
import VueUniversalModal from "vue-universal-modal";
import "vue-universal-modal/dist/index.css";

app.use(VueUniversalModal, {
  teleportTarget: "#modals",
  modalComponent: "Modal",
});
// -- END --

// --START-- Import plugin modal vue3-toastify
// https://vue3-toastify.js-bridge.com/api/toast.html
// https://github.com/jerrywu001/vue3-toastify
import Vue3Toastify, { type ToastContainerOptions } from "vue3-toastify";

app.use(Vue3Toastify, {
  autoClose: 3000,
} as ToastContainerOptions);
// -- END --

// app.config.isCustomElement = tag => tag.startsWith("xml")
app.mount("#app");
