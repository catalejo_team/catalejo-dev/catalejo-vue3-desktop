import { fileURLToPath, URL } from "url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

//import native from "vite-plugin-native";

/*
 * https://github.com/davidmyersdev/vite-plugin-node-polyfills
 * Externalizar modulos como serialport
 */
// import { nodePolyfills } from "vite-plugin-node-polyfills";

import { viteStaticCopy } from "vite-plugin-static-copy";

// import { nodePolyfills } from "vite-plugin-node-polyfills";

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    host: "0.0.0.0",
  },
  base: "", // ruta relativa para la construcción del dist
  // build: {
  //   rollupOptions: {
  //     external: ["serialport"],
  //   },
  // },
  // optimizeDeps: {
  //   exclude: ["serialport"],
  // },
  plugins: [
    vue({
      template: {
        compilerOptions: { // agregar tipos para que en el debug sea reconocido
          isCustomElement: tag =>
            tag === 'xml' || tag === 'field' || tag === 'block' ||
            tag === 'value' || tag === 'statement' || tag === 'mutation' ||
            tag === 'arg' || tag === 'variables' || tag === 'variable'
        }
      }
    }),
    viteStaticCopy({
      targets: [
        {
          src: "node_modules/@johnnycubides/blockly-catalejo/media", // Agregando rutas estaticas al copiar
          dest: "",
        },
        {
          src: "src/blockly-config/luna8266/imag",
          dest: "img",
        },
        {
          src: "src/blockly-config/chuck/chuckImg",
          dest: "img",
        },
        {
          src: "../../docs/catalejo-chuck-docs/build/*",
          dest: "chuck-docs",
        },
        {
          src: "../../docs/catalejo-luabot-esp8266-docs/build/*",
          dest: "luna8266-docs",
        },
      ],
    }),
    // native({}),
    // nodePolyfills({
    //   // To exclude specific polyfills, add them to this list.
    //   exclude: [
    //     "fs", "serialport", // Excludes the polyfill for `fs` and `node:fs`.
    //   ],
    //   // Whether to polyfill specific globals.
    //   globals: {
    //     Buffer: true, // can also be 'build', 'dev', or false
    //     global: true,
    //     process: true,
    //   },
    //   // Whether to polyfill `node:` protocol imports.
    //   protocolImports: true,
    // }),
    // nodePolyfills({
    //   // To exclude specific polyfills, add them to this list.
    //   exclude: [
    //   ],
    //   // Whether to polyfill specific globals.
    //   globals: {
    //     Buffer: false, // can also be 'build', 'dev', or false
    //     global: false,
    //     process: false,
    //   },
    //   // Whether to polyfill `node:` protocol imports.
    //   protocolImports: true,
    // }),
  ],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
});
