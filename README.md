# catalejo-vue3-desktop

## Compilar la vue3 app

### Compilar desde npm

```bash
npm run dev
npm run build-only
```

### Compilar con make

En fase de desarrollo se hace uso de `make d`, cuando se ejecuta este comando
puede visualizar el resultado en un navegador web; Tener presente que puede hacer uso
de nwjs, electron o newtralinojs para crear la aplicación con los servicios que interactuan
con el sistema como es el caso de manejo de archivos o comunicaciones TCP diferentes a aquellas
ofrecidas por los protocolos web desde navegadores.

El comando `make b` entrega el producto empaquetado y listo para producción según objetivo
(newtralinojs, electron, nwjs).

```bash
make d # Launch vite debug tool
make b # Build vue3 app
```

## Upgrade package in project

```bash
npm-check-updates
ncu -u
npm install
```

## Resumen de algunas actividades realizadas en el inicio del desarrollo

npm init vue@latest

Vue.js - The Progressive JavaScript Framework

```bash
✔ Project name: … catalejo-vue3-desktop
✔ Add TypeScript? … No / Yes
✔ Add JSX Support? … No / Yes
✔ Add Vue Router for Single Page Application development? … No / Yes
✔ Add Pinia for state management? … No / Yes
✔ Add Vitest for Unit Testing? … No / Yes
✔ Add Cypress for both Unit and End-to-End testing? … No / Yes
✔ Add ESLint for code quality? … No / Yes
✔ Add Prettier for code formatting? … No / Yes
```

Scaffolding project in /home/johnny/projects/catalejo-editor-desktop/catalejo-desktop/catalejo-vue3-desktop/catalejo-vue3-desktop...

Done. Now run:

```bash
cd catalejo-vue3-desktop
npm install
npm run lint
npm run dev
```

```bash
npm i vue-universal-modal
npm i blockly --save-dev
npm audit fix --force
npm i --save @fortawesome/fontawesome-svg-core @fortawesome/free-solid-svg-icons @fortawesome/vue-fontawesome@prerelease @fortawesome/free-brands-svg-icons @fortawesome/free-regular-svg-icons
npm i vite-plugin-static-copy # genera error, al ejecutar la siguiente línea se resuelve
npm i -D vite-plugin-static-copy --legacy-peer-deps # problemas con dependencias
npm install chrome-apps-serialport
```

## Cómo construir

1. Entrar al directorio ../../catalejo-oursblockly/ y construir las librerías propias de chuck y nodemcu
2. 

## References

https://vue-styleguidist.github.io/docs/Documenting.html#slots

https://vuejs.org/api/

https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html

https://www.typescriptlang.org/docs/handbook/


